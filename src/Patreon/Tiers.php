<?php


namespace RCSI\Patreon;


use RCSI\Exceptions\PatreonCacheException;
use RCSI\Exceptions\PatreonDataException;

class Tiers extends Component
{
    protected $cacheFile = 'tiers.patreon.json';
    protected $campaign = [];
    protected $tiers = [];

    public function run()
    {
        try {
            $this->tiers = $this->getCache();
        } catch (PatreonCacheException $pe) {
            if (empty($this->campaign)) {
                throw new PatreonDataException("Data has not been loaded");
            }

            foreach ($this->campaign['included'] as $tier){
                $this->tiers[$tier['id']] = array(
                    'amount' => $tier['attributes']['amount_cents'],
                    'title' => $tier['attributes']['title']
                );
            }
            $this->saveCache($this->tiers);

        }
        return $this->tiers;
    }

    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
        return $this;
    }
}