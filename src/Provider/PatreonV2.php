<?php


namespace RCSI\Provider;


use Gravure\Patreon\Oauth\Provider\Patreon;

class PatreonV2 extends Patreon
{
    protected $scopes = [
        'identity', 'identity.memberships', 'identity[email]',
    ];

    protected function getDefaultScopes()
    {
        return ['identity', 'identity[email]', 'identity.memberships'];
    }

    public function setScopes(array $scopes)
    {
        $this->scopes = $scopes;
    }

    public function getAPIBaseURI()
    {
        return $this->apiBaseUrl;
    }
}