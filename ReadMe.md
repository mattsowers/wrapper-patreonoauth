# Patreon-OAuth Wrapper

This package is a shortcut to set up authentication for Patreon's OAuth on their API v2.
It will handle OAuth verification, and then make API calls to marry up user data with support tiers.

Code is simple:



    $patreon = \RCSI\Auth\PatreonAuth::init();
    $patreon  
        ->setClientID($patreonAPIClientID)  
        ->setClientSecret($patreonAPIClientSecret)  
        ->setRedirectURI($patreonAPIRedirectURI)  
        ->setCreatorAccessToken($patreonCreatorAccessToken);  
    $patreon->auth();
    
    $userdata = $patreon->getUserDetails();  
    $isLoggedIn = $patreon->getIsLoggedIn();
    $isPatreon = $patreon->getIsPatreon();
    $pledgeLevel = $patreon->getPledgeLevel();
    $tierName = $patreon->getTierName();

If you're using rcsi/filesystem-config as a configuration tool, its even simpler

    $config = \RCSI\Wrapper\ConfigWrapper::init();
    $patreon = \RCSI\Auth\PatreonAuth::init($config);
    $patreon->auth();
    
    $userdata = $patreon->getUserDetails();  
    $isLoggedIn = $patreon->getIsLoggedIn();
    $isPatreon = $patreon->getIsPatreon();
    $pledgeLevel = $patreon->getPledgeLevel();
    $tierName = $patreon->getTierName();

To trigger the OAuth Login sequence, you'll need to put a GET request with:

    https://your-url.xyz/?go=go

To logout it's a similar GET

    https://your-url.xyz/?logout=logout





